package com.example.starwars.unit;

import com.example.starwars.core.AbstractUnitTest;
import com.example.starwars.data.repository.RebeldeRepository;
import com.example.starwars.data.service.impl.RebeldeServiceImpl;
import com.example.starwars.data.service.validators.ObterPontosRebeldeImpl;
import com.example.starwars.domain.model.Rebelde;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.starwars.core.mocks.SetupMocks.setupListRebelde;
import static com.example.starwars.core.mocks.SetupMocks.setupLocalizacao;
import static com.example.starwars.core.mocks.SetupMocks.setupRebelde;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;


class RebeldeUnitTest extends AbstractUnitTest {

    @Mock
    RebeldeRepository repository;

    @Mock
    ObterPontosRebeldeImpl obterPontosRebelde;

    @InjectMocks
    RebeldeServiceImpl service;

    @Test
    void deveSalvarUmRebelde() {
        service.criar(setupRebelde());
        verify(repository, times(1)).save(setupRebelde());
    }

    @Test
    void deveReportarUmRebelde() {
        Rebelde mock = setupRebelde();
        service.criar(mock);
        when(repository.findById(mock.getId())).thenReturn(Optional.of(mock));
        service.reportar(mock.getId());
        Assert.assertEquals(repository.findById(mock.getId()).get().getReportCont(), 1);
    }

    @Test
    void deveListarTodosOsRebeldes() {
        List<Rebelde> rebeldeList = new ArrayList<>();
        rebeldeList.add(setupRebelde());
        rebeldeList.add(setupRebelde());
        for (Rebelde e : rebeldeList) {
            service.criar(e);
        }
        service.listar();
        when(repository.findAll()).thenReturn(setupListRebelde(rebeldeList));
    }

    @Test
    void deveAtualizarLocalizacaoRebelde() {
        Rebelde mock = setupRebelde();
        service.criar(mock);
        when(repository.findById(mock.getId())).thenReturn(Optional.of(mock));
        service.atualizarLocalizacao(mock.getId(), setupLocalizacao());
        Assert.assertEquals(repository.findById(mock.getId()).get().getLocalizacao().getNomeGalaxia(), "Andromeda");
    }

    @Test
    void deveRecuperarUmRebeldePeloId() {
        Rebelde mock = setupRebelde();
        service.criar(mock);
        when(repository.findById(mock.getId())).thenReturn(Optional.of(mock));
        service.recuperarPorId(mock.getId());
        Assert.assertEquals(repository.findById(mock.getId()).get().getId(), mock.getId());
    }

    @Test
    void deveAtualizarUmRebelde() {
        Rebelde mock = setupRebelde();
        service.criar(mock);
        when(repository.findById(mock.getId())).thenReturn(Optional.of(mock));
        Rebelde mock2 = setupRebelde(2L, "Gabriela Vitoria");
        service.atualizar(mock.getId(), mock2);
        when(repository.findById(mock2.getId())).thenReturn(Optional.of(mock2));
        Assert.assertEquals(repository.findById(mock2.getId()).get().getId(), mock2.getId());
    }
}
