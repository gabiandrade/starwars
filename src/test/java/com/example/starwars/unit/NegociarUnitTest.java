package com.example.starwars.unit;


import com.example.starwars.core.AbstractUnitTest;
import com.example.starwars.data.repository.RebeldeRepository;
import com.example.starwars.data.service.impl.NegociarItensServiceImpl;
import com.example.starwars.data.service.impl.RebeldeServiceImpl;
import com.example.starwars.data.service.validators.ObterPontosRebeldeImpl;
import com.example.starwars.domain.enums.ItemTipo;
import com.example.starwars.domain.model.Item;
import com.example.starwars.domain.model.Rebelde;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.example.starwars.core.mocks.SetupMocks.setupRebelde;
import static org.mockito.Mockito.when;

class NegociarUnitTest extends AbstractUnitTest {

    @Mock
    RebeldeRepository repository;

    @Mock
    ObterPontosRebeldeImpl obterPontosRebelde;

    @InjectMocks
    NegociarItensServiceImpl service;

    @InjectMocks
    RebeldeServiceImpl rebeldeService;


    @Test
    void deveNegociarItensEntreRebeldes() {
        Rebelde mock1 = setupRebelde(1L, "Luke");
        Rebelde mock2 = setupRebelde(2L, "Anakin");

        List<Item> listItensRebelde1 = new ArrayList<>();
        listItensRebelde1.add(new Item(ItemTipo.ARMA, 1));
        listItensRebelde1.add(new Item(ItemTipo.COMIDA, 1));

        List<Item> listItensRebelde2 = new ArrayList<>();
        listItensRebelde2.add(new Item(ItemTipo.AGUA, 1));
        listItensRebelde2.add(new Item(ItemTipo.MUNICAO, 1));
        rebeldeService.criar(mock1);
        rebeldeService.criar(mock2);
        when(repository.findById(mock1.getId())).thenReturn(Optional.of(mock1));
        when(repository.findById(mock2.getId())).thenReturn(Optional.of(mock2));
        service.negociarItens(mock1.getId(), mock2.getId(), listItensRebelde1, listItensRebelde2);

        Assert.assertEquals(repository.findById(mock1.getId()).get().getInventario().getItens().get(0).getNome(), ItemTipo.MUNICAO);
        Assert.assertEquals(repository.findById(mock1.getId()).get().getInventario().getItens().get(1).getNome(), ItemTipo.AGUA);
        Assert.assertEquals(repository.findById(mock2.getId()).get().getInventario().getItens().get(0).getNome(), ItemTipo.ARMA);
        Assert.assertEquals(repository.findById(mock2.getId()).get().getInventario().getItens().get(1).getNome(), ItemTipo.COMIDA);
    }

}
