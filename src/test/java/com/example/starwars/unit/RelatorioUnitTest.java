package com.example.starwars.unit;

import com.example.starwars.core.AbstractUnitTest;
import com.example.starwars.data.repository.RebeldeRepository;
import com.example.starwars.data.service.impl.RelatorioServiceImpl;
import com.example.starwars.domain.dto.Relatorio;
import com.example.starwars.domain.model.Rebelde;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.util.ArrayList;
import java.util.List;

import static com.example.starwars.core.mocks.SetupMocks.setupRebelde;
import static com.example.starwars.core.mocks.SetupMocks.setupRebeldeTraidor;
import static org.mockito.Mockito.when;

class RelatorioUnitTest extends AbstractUnitTest {

    @InjectMocks
    RelatorioServiceImpl service;

    @Mock
    RebeldeRepository repository;


    @Test
    void deveGerarORelatorioDePorcentagemDeTraidores(){
        List<Rebelde> rebeldeList = new ArrayList<>();
        rebeldeList.add(setupRebeldeTraidor(1L, "Luke SkyWalker",3));
        rebeldeList.add(setupRebeldeTraidor(2L, "Anakin SkyWalker",3));
        Relatorio relatorioMock = new Relatorio();
        relatorioMock.setPorcentagemTraidores(100.0);
        when(repository.findAll()).thenReturn(rebeldeList);
       Assert.assertEquals(service.gerarRelatorioPorcentagemTraidores().getPorcentagemTraidores(), relatorioMock.getPorcentagemTraidores());
    }

    @Test
    void deveGerarRelatorioPorcentagemRebeldes(){
        List<Rebelde> rebeldeList = new ArrayList<>();
        rebeldeList.add(setupRebelde(1L, "Luke SkyWalker"));
        rebeldeList.add(setupRebelde(2L, "Anakin SkyWalker"));
        Relatorio relatorioMock = new Relatorio();
        relatorioMock.setPorcentagemRebeldes(100.0);
        when(repository.findAll()).thenReturn(rebeldeList);
        Assert.assertEquals(service.gerarRelatorioPorcentagemRebeldes().getPorcentagemRebeldes(), relatorioMock.getPorcentagemRebeldes());
    }

    @Test
    void deveGerarRelatorioDePontosPerdidos(){
        List<Rebelde> rebeldeList = new ArrayList<>();
        rebeldeList.add(setupRebeldeTraidor(1L, "Luke SkyWalker",3));
        rebeldeList.add(setupRebeldeTraidor(2L, "Anakin SkyWalker",3));
        Relatorio relatorioMock = new Relatorio();
        relatorioMock.setPontosPerdidos(20);
        when(repository.findAll()).thenReturn(rebeldeList);
        Assert.assertEquals(service.gerarRelatorioPontosPerdidos().getPontosPerdidos(), relatorioMock.getPontosPerdidos());
    }

    @Test
    void deveGerarRelatorioDeRecursos(){
        List<Rebelde> rebeldeList = new ArrayList<>();
        rebeldeList.add(setupRebelde(1L, "Luke SkyWalker"));
        rebeldeList.add(setupRebelde(2L, "Anakin SkyWalker"));
        Relatorio relatorioMock = new Relatorio();
        relatorioMock.setQtdMediaAgua(1.0);
        relatorioMock.setQtdMediaArma(1.0);
        relatorioMock.setQtdMediaComida(1.0);
        relatorioMock.setQtdMediaMunicao(1.0);
        when(repository.findAll()).thenReturn(rebeldeList);
        Assert.assertEquals(service.gerarRelatorioRecursos().getQtdMediaAgua(),
                relatorioMock.getQtdMediaAgua());

        Assert.assertEquals(service.gerarRelatorioRecursos().getQtdMediaArma(),
                relatorioMock.getQtdMediaArma());

        Assert.assertEquals(service.gerarRelatorioRecursos().getQtdMediaComida(),
                relatorioMock.getQtdMediaComida());

        Assert.assertEquals(service.gerarRelatorioRecursos().getQtdMediaMunicao(),
                relatorioMock.getQtdMediaMunicao());

    }
}
