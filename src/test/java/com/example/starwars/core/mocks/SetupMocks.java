package com.example.starwars.core.mocks;

import com.example.starwars.domain.enums.Genero;
import com.example.starwars.domain.enums.ItemTipo;
import com.example.starwars.domain.model.Inventario;
import com.example.starwars.domain.model.Item;
import com.example.starwars.domain.model.Localizacao;
import com.example.starwars.domain.model.Rebelde;

import java.util.ArrayList;
import java.util.List;

public class SetupMocks {

    public static Rebelde setupRebelde(){
        Rebelde rebelde1 = new Rebelde();
        rebelde1.setId(1L);
        rebelde1.setNome("Luke Skywalker");
        rebelde1.setGenero(Genero.M);
        rebelde1.setIdade(18);
        Inventario inventario1 = new Inventario();
        List<Item> itens = new ArrayList<Item>();
        itens.add(new Item(ItemTipo.ARMA, 2));
        itens.add(new Item(ItemTipo.MUNICAO, 40));
        itens.add(new Item(ItemTipo.AGUA, 10));
        itens.add(new Item(ItemTipo.COMIDA, 20));
        inventario1.setItens(itens);
        rebelde1.setInventario(inventario1);
        return rebelde1;
    }
    public static Rebelde setupRebelde(Long id, String nome){
        Rebelde rebelde1 = new Rebelde();
        rebelde1.setId(id);
        rebelde1.setNome(nome);
        rebelde1.setGenero(Genero.M);
        rebelde1.setIdade(18);
        Inventario inventario1 = new Inventario();
        List<Item> itens = new ArrayList<Item>();
        itens.add(new Item(ItemTipo.ARMA, 1));
        itens.add(new Item(ItemTipo.MUNICAO, 1));
        itens.add(new Item(ItemTipo.AGUA, 1));
        itens.add(new Item(ItemTipo.COMIDA, 1));
        inventario1.setItens(itens);
        rebelde1.setInventario(inventario1);
        return rebelde1;
    }

    public static Rebelde setupRebeldeTraidor(Long id, String nome, Integer count){
        Rebelde rebelde1 = new Rebelde();
        rebelde1.setId(id);
        rebelde1.setNome(nome);
        rebelde1.setGenero(Genero.M);
        rebelde1.setIdade(18);
        rebelde1.setReportCont(count);
        Inventario inventario1 = new Inventario();
        List<Item> itens = new ArrayList<Item>();
        itens.add(new Item(ItemTipo.ARMA, 1));
        itens.add(new Item(ItemTipo.MUNICAO, 1));
        itens.add(new Item(ItemTipo.AGUA, 1));
        itens.add(new Item(ItemTipo.COMIDA, 1));
        inventario1.setItens(itens);
        inventario1.setTotalPontos(10);
        rebelde1.setInventario(inventario1);
        return rebelde1;
    }

    public static List<Rebelde> setupListRebelde(Rebelde rebelde){
        List<Rebelde> list = new ArrayList<Rebelde>();
        list.add(rebelde);
        return list;
    }
    public static List<Rebelde> setupListRebelde(List<Rebelde> rebelde){
        return rebelde;
    }

    public static Localizacao setupLocalizacao(){
        Localizacao locate = new Localizacao();
        locate.setId(1L);
        locate.setLongitude(127327F);
        locate.setLatitude(13123123F);
        locate.setNomeGalaxia("Andromeda");
        return locate;
    }

    public static List<Item> setupListItens(Item item){
        List<Item> itens = new ArrayList<Item>();
        itens.add(item);
        return itens;
    }
}