package com.example.starwars.core;

import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractUnitTest {

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
    }
}