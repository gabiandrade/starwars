package com.example.starwars.data.service.impl;

import com.example.starwars.data.service.inter.INegociarItensService;
import com.example.starwars.data.service.validators.ObterPontosRebeldeImpl;
import com.example.starwars.domain.exception.BadRequestException;
import com.example.starwars.domain.exception.NotFoundException;
import com.example.starwars.domain.model.Item;
import com.example.starwars.domain.model.Rebelde;
import com.example.starwars.data.repository.RebeldeRepository;
import com.example.starwars.commons.utils.Mensagem;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@AllArgsConstructor
@Service
public class NegociarItensServiceImpl implements INegociarItensService {

    private final RebeldeRepository repository;
    @Autowired
    private final ObterPontosRebeldeImpl obterPontosRebelde;

    @Override
    public void negociarItens(Long rebeldeId1, Long rebeldeId2, List<Item> itensMembroRebelde1,
                         List<Item> itensMembroRebelde2) {

        Optional<Rebelde> membroRebelde1 = repository.findById(rebeldeId1);
        if (membroRebelde1.isPresent()) {
            verificarRebeldeTraidor(membroRebelde1);
        } else {
            throw new NotFoundException(Mensagem.REBELDE_NAO_ENCONTRADO);
        }

        Optional<Rebelde> membroRebelde2 = repository.findById(rebeldeId2);
        if (membroRebelde2.isPresent()) {
            verificarRebeldeTraidor(membroRebelde2);
        }

        if (obterPontosRebelde.obterPontos(itensMembroRebelde1) !=
                obterPontosRebelde.obterPontos(itensMembroRebelde2)) {
            throw new BadRequestException(Mensagem.QTD_PONTOS_DIFERENTES);
        }

        verificarItens(membroRebelde1, itensMembroRebelde1);
        verificarItens(membroRebelde2, itensMembroRebelde2);

        substituirItens(membroRebelde1, itensMembroRebelde1, itensMembroRebelde2);
        substituirItens(membroRebelde2, itensMembroRebelde2, itensMembroRebelde1);

        membroRebelde1.ifPresent(repository::save);
        membroRebelde2.ifPresent(repository::save);
    }

    private void verificarRebeldeTraidor(Optional<Rebelde> rebelde) {
        if (rebelde.isPresent() && rebelde.get().getReportCont() >= 3) {
            throw new BadRequestException(Mensagem.REBELDE_TRAIDOR.replace("{rebelde}",
                    rebelde.get().getNome()));
        }
    }


    private void verificarItens(Optional<Rebelde> rebelde, List<Item> itens) {
        if (rebelde.isPresent() && obterPontosRebelde.obterPontos(itens) >
                obterPontosRebelde.obterPontos(rebelde.get().getInventario().getItens())) {
            throw new BadRequestException(Mensagem.ITENS_REBELDE_EXCEDE
                    .replace("{rebelde}", rebelde.get().getNome()));
        }
    }

    private void substituirItens(Optional<Rebelde> rebelde, List<Item> itensParaRemover,
                                 List<Item> itensParaAdicionar) {

        if (rebelde.isPresent()) {
            List<Item> itensAtuais = rebelde.get().getInventario().getItens();

            Iterator<Item> itensAtuaisIterator = itensAtuais.iterator();

            while (itensAtuaisIterator.hasNext()) {
                Item itemAtual = itensAtuaisIterator.next();
                for (Item itemParaRemover : itensParaRemover) {
                    if (itemAtual.getNome().equals(itemParaRemover.getNome())) {
                        if (itemParaRemover.getQuantidade() > itemAtual.getQuantidade())
                            throw new BadRequestException(Mensagem
                                    .QTD_ITEM_TROCA_MAIOR_QUE_INVETARIO.replace("{item}",
                                            itemParaRemover.getNome().name()));
                        int qtd = itemAtual.getQuantidade() - itemParaRemover.getQuantidade();
                        if (qtd == 0) {
                            itensAtuaisIterator.remove();
                            continue;
                        }
                        itemAtual.setQuantidade(qtd);
                    }
                }
            }

            // Adicionar Itens
            for (Item itemParaAdicionar : itensParaAdicionar) {
                Item item = itensAtuais.stream().filter(itemAtual -> itemAtual.getNome().equals(itemParaAdicionar.getNome())).findAny().orElse(null);
                if (item != null)
                    item.setQuantidade(item.getQuantidade() + itemParaAdicionar.getQuantidade());
                else
                    itensAtuais.add(itemParaAdicionar);
            }
        }
    }
}
