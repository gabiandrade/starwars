package com.example.starwars.data.service.impl;

import com.example.starwars.commons.utils.Mensagem;
import com.example.starwars.data.repository.RebeldeRepository;
import com.example.starwars.data.service.inter.IRelatorioService;
import com.example.starwars.domain.dto.Relatorio;
import com.example.starwars.domain.exception.NotFoundException;
import com.example.starwars.domain.model.Item;
import com.example.starwars.domain.model.Rebelde;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class RelatorioServiceImpl implements IRelatorioService {

    @Autowired
    private RebeldeRepository repository;

    @Override
    public Relatorio gerarRelatorioPorcentagemTraidores() {
        List<Rebelde> rebeldes = repository.findAll();
        int contTraidor = 0;

        for (Rebelde rebelde : rebeldes) {

            if (rebelde.getReportCont() >= 3) {
                contTraidor++;
            }

        }
        Relatorio relatorio = new Relatorio();
        Double porcentagemTraidor = ((double) contTraidor * 100) / rebeldes.size();
        relatorio.setPorcentagemTraidores(porcentagemTraidor);
        return relatorio;
    }

    @Override
    public Relatorio gerarRelatorioPorcentagemRebeldes() {

        List<Rebelde> rebeldes = repository.findAll();
        int contRebelde = 0;
        for (Rebelde rebelde : rebeldes) {

            if (rebelde.getReportCont() < 3) {
                contRebelde++;
            }
        }
        Relatorio relatorio = new Relatorio();
        Double porcentagemRebelde = ((double) contRebelde * 100) / rebeldes.size();
        relatorio.setPorcentagemRebeldes(porcentagemRebelde);
        return relatorio;
    }

    @Override
    public Relatorio gerarRelatorioRecursos() {

        List<Rebelde> rebeldes = repository.findAll();
        double contAgua = 0;
        double contComida = 0;
        double contArma = 0;
        double contMunicao = 0;
        int contRebelde = 0;

        for (Rebelde rebelde : rebeldes) {
            if (rebelde.getReportCont() < 3) {
                contRebelde++;
                for (Item item : rebelde.getInventario().getItens()) {
                    switch (item.getNome()) {
                        case ARMA:
                            contArma += item.getQuantidade();
                            break;
                        case MUNICAO:
                            contMunicao += item.getQuantidade();
                            break;
                        case AGUA:
                            contAgua += item.getQuantidade();
                            break;
                        case COMIDA:
                            contComida += item.getQuantidade();
                            break;
                    }
                }
            }
        }

        if (contRebelde == 0) {
            throw new NotFoundException(Mensagem.RECURSO_INACESSIVEL);
        }

        Relatorio relatorio = new Relatorio();
        Double qtdMediaAgua = contAgua / (double) contRebelde;
        Double qtdMediaComida = contComida / (double) contRebelde;
        Double qtdMediaArmas = contArma / (double) contRebelde;
        Double qtdMediaMunicao = contMunicao / (double) contRebelde;
        relatorio.setQtdMediaAgua(qtdMediaAgua);
        relatorio.setQtdMediaComida(qtdMediaComida);
        relatorio.setQtdMediaArma(qtdMediaArmas);
        relatorio.setQtdMediaMunicao(qtdMediaMunicao);
        return relatorio;
    }

    @Override
    public Relatorio gerarRelatorioPontosPerdidos() {

        List<Rebelde> rebeldes = repository.findAll();
        int contPontosPerdidos = 0;
        for (Rebelde rebelde : rebeldes) {
            if (rebelde.getReportCont() >= 3) {
                contPontosPerdidos += rebelde.getInventario().getTotalPontos();
            }
        }

        Relatorio relatorio = new Relatorio();
        relatorio.setPontosPerdidos(contPontosPerdidos);
        return relatorio;
    }
}
