package com.example.starwars.data.service.inter;

import com.example.starwars.domain.model.Item;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public interface IObterPontosRebelde {
    int obterPontos(List<Item> itens);
}
