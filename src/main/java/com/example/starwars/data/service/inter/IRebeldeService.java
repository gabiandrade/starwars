package com.example.starwars.data.service.inter;

import com.example.starwars.domain.model.Localizacao;
import com.example.starwars.domain.model.Rebelde;

import java.util.List;

public interface IRebeldeService {

    Rebelde criar(Rebelde rebelde);

    Rebelde atualizar(Long rebeldeId, Rebelde rebelde);

    void atualizarLocalizacao(Long rebeldeId, Localizacao localizacao);

    List<Rebelde> listar();

    Rebelde recuperarPorId(Long rebeldeId);

    void reportar(Long rebeldeId);
}
