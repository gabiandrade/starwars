package com.example.starwars.data.service.impl;

import com.example.starwars.commons.utils.Mensagem;
import com.example.starwars.data.repository.RebeldeRepository;
import com.example.starwars.data.service.inter.IObterPontosRebelde;
import com.example.starwars.data.service.inter.IRebeldeService;
import com.example.starwars.domain.exception.BadRequestException;
import com.example.starwars.domain.exception.ConflictException;
import com.example.starwars.domain.exception.NotFoundException;
import com.example.starwars.domain.model.Item;
import com.example.starwars.domain.model.Localizacao;
import com.example.starwars.domain.model.Rebelde;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RebeldeServiceImpl implements IRebeldeService {

    @Autowired
    private RebeldeRepository repository;

    @Autowired
    private IObterPontosRebelde obterPontosRebelde;

    @Override
    public Rebelde criar(Rebelde rebelde) {
        if (rebelde.getInventario() == null)
            throw new BadRequestException("Rebelde deve ter um inventário!");

        List<Item> itens = rebelde.getInventario().getItens();

        if (itens == null || itens.isEmpty())
            throw new BadRequestException("Rebelde deve ter no mínimo um item no seu inventário!");

        rebelde.setReportCont(0);

        rebelde.getInventario().setTotalPontos(obterPontosRebelde.obterPontos(rebelde.getInventario().getItens()));

        return repository.save(rebelde);
    }

    @Override
    public Rebelde atualizar(Long id, Rebelde rebelde) {
        Optional<Rebelde> rebeldeAtualizado = repository.findById(id);
        if (rebeldeAtualizado.isEmpty()) {
            throw new NotFoundException(Mensagem.REBELDE_NAO_ENCONTRADO);
        }

        if (rebeldeAtualizado.get().getNome().equals(rebelde.getNome())) {
            throw new ConflictException("Rebelde já existe");
        }

        BeanUtils.copyProperties(rebelde, rebeldeAtualizado);
        rebeldeAtualizado.get().setId(id);
        return repository.save(rebeldeAtualizado.get());
    }

    @Override
    public List<Rebelde> listar() {
        return repository.findAll();
    }

    @Override
    public Rebelde recuperarPorId(Long rebeldeId) {
        Optional<Rebelde> rebelde = repository.findById(rebeldeId);

        if (rebelde.isEmpty()) {
            throw new NotFoundException(Mensagem.REBELDE_NAO_ENCONTRADO);
        }

        return rebelde.get();
    }

    @Override
    public void atualizarLocalizacao(Long id, Localizacao localizacao) {
        Optional<Rebelde> rebelde = repository.findById(id);
        if (rebelde.isEmpty()) {
            throw new NotFoundException(Mensagem.REBELDE_NAO_ENCONTRADO);
        }
        rebelde.get().setLocalizacao(localizacao);

        repository.save(rebelde.get());
    }

    @Override
    public void reportar(Long id) {
        Optional<Rebelde> rebelde = repository.findById(id);
        if (rebelde.isEmpty()) {
            throw new NotFoundException(Mensagem.REBELDE_NAO_ENCONTRADO);
        }
        rebelde.get().setReportCont(rebelde.get().getReportCont() + 1);
        repository.save(rebelde.get());
    }

}
