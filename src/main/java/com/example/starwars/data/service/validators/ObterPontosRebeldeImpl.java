package com.example.starwars.data.service.validators;

import com.example.starwars.data.service.inter.IObterPontosRebelde;
import com.example.starwars.domain.model.Item;
import org.springframework.stereotype.Component;

import java.util.List;
@Component
public class ObterPontosRebeldeImpl implements IObterPontosRebelde {

    public int obterPontos(List<Item> itens) {
        int pontos = 0;
        for (Item item : itens) {

            switch (item.getNome()) {
                case ARMA:
                    pontos += 4 * item.getQuantidade();
                    break;
                case MUNICAO:
                    pontos += 3 * item.getQuantidade();
                    break;
                case AGUA:
                    pontos += 2 * item.getQuantidade();
                    break;
                case COMIDA:
                    pontos += item.getQuantidade();
                    break;
            }
        }

        return pontos;
    }
}
