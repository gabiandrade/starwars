package com.example.starwars.data.service.inter;

import com.example.starwars.domain.model.Item;

import java.util.List;

public interface INegociarItensService {

    void negociarItens(Long rebeldeId1, Long rebeldeId2,
                  List<Item> itensMembroRebelde1, List<Item> itensMembroRebelde2);
}
