package com.example.starwars.data.service.inter;

import com.example.starwars.domain.dto.Relatorio;

public interface IRelatorioService {

    Relatorio gerarRelatorioPorcentagemTraidores();
    Relatorio gerarRelatorioPorcentagemRebeldes();
    Relatorio gerarRelatorioRecursos();
    Relatorio gerarRelatorioPontosPerdidos();
}
