package com.example.starwars.domain.exception.validation;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@JsonSerialize
public class ErrorObject {

    private final String message;
    private final String field;
    private final Object parameter;
}
