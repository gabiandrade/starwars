package com.example.starwars.domain.exception;

public class ConflictException extends RuntimeException {

    public ConflictException(String message, Object ... args) {
        super(String.format(message, args));
    }
}