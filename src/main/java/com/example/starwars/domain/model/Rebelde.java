package com.example.starwars.domain.model;

import com.example.starwars.domain.enums.Genero;
import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;


@Getter
@Setter
@Entity(name = "rebelde")
@EqualsAndHashCode(of = "id")
public class Rebelde {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String nome;

    @NotNull
    private int idade;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Genero genero;

    private int reportCont;

    @OneToOne(cascade = CascadeType.ALL)
    private Localizacao localizacao;

    @OneToOne(cascade = CascadeType.ALL)
    private Inventario inventario;
}
