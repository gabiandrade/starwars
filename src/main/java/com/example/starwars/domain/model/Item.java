package com.example.starwars.domain.model;

import com.example.starwars.domain.enums.ItemTipo;
import com.sun.istack.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@NoArgsConstructor
@Entity(name="item")
public class Item {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ItemTipo nome;

    @NotNull
    private int quantidade;

    public Item(ItemTipo nome, int quantidade) {
        this.nome = nome;
        this.quantidade = quantidade;
    }
}
