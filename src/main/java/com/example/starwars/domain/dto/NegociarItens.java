package com.example.starwars.domain.dto;

import com.example.starwars.domain.model.Item;
import lombok.Getter;

import java.util.List;

@Getter
public class NegociarItens {

    private Long rebeldeId1;
    private List<Item> itensMembroRebelde1;
    private Long rebeldeId2;
    private List<Item> itensMembroRebelde2;
}
