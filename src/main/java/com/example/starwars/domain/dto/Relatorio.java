package com.example.starwars.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Relatorio {

    private Double porcentagemTraidores;
    private Double porcentagemRebeldes;
    private Double qtdMediaAgua;
    private Double qtdMediaComida;
    private Double qtdMediaArma;
    private Double qtdMediaMunicao;
    private Integer pontosPerdidos;
}
