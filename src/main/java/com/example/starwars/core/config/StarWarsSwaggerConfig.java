package com.example.starwars.core.config;

import com.example.starwars.core.config.utils.Conf;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;


@Configuration
@EnableSwagger2
public class StarWarsSwaggerConfig extends WebMvcConfigurationSupport {

    @Bean
    public Docket getApiClient() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage(
                Conf.SWAGGER_BASE_PACKAGE
        )).build().apiInfo(setupApiInfo());
    }

    public ApiInfo setupApiInfo() {
        return new ApiInfoBuilder().title(
                Conf.SWAGGER_API_INFO_TITLE
        ).description(Conf.SWAGGER_API_INFO_DESCRIPTION).version(Conf.SWAGGER_API_INFO_VERSION).contact(new Contact(
                Conf.SWAGGER_API_CONTACT_AUTHOR,
                Conf.SWAGGER_API_CONTACT_EMAIL,
                Conf.SWAGGER_API_CONTACT_URL
        )).build();
    }

    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
        super.addResourceHandlers(registry);
    }
}
