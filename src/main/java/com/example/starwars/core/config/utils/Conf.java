package com.example.starwars.core.config.utils;

public class Conf {

    /**
     * CONFIGURATIONS SWAGGER
     */
    public static String SWAGGER_BASE_PACKAGE = "com.example.starwars";
    public static String SWAGGER_API_INFO_TITLE = "StarWars API";
    public static String SWAGGER_API_INFO_DESCRIPTION = "Challenge StarWars API WEB CLIENT";
    public static String SWAGGER_API_INFO_VERSION = "1.0.0";
    public static final String SWAGGER_API_CONTACT_EMAIL = "gabriela.vitoria@dcx.ufpb.br";
    public static final String SWAGGER_API_CONTACT_AUTHOR = "Gabriela Vitoria";
    public static final String SWAGGER_API_CONTACT_URL = "https://gitlab.com/gabiandrade/";
}
