package com.example.starwars.view.controller;

import com.example.starwars.domain.dto.NegociarItens;
import com.example.starwars.data.service.impl.NegociarItensServiceImpl;
import com.example.starwars.commons.utils.Mensagem;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/negociar-itens", produces = MediaType.APPLICATION_JSON_VALUE)
@ApiOperation(value = "Market Place - Adversiting")
public class NegociarItensController {

    private final NegociarItensServiceImpl negociarItensServiceImpl;

    @PostMapping
    @ApiOperation(value = "Permite negociar itens entre rebeldes")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Troca de Itens foi um sucesso"),
            @ApiResponse(code = 400, message = "A troca não foi realizada com sucesso"),
    })
    public ResponseEntity<?> negociarItens(@RequestBody NegociarItens negociarItens) {
        try {
            negociarItensServiceImpl.negociarItens(negociarItens.getRebeldeId1(), negociarItens.getRebeldeId2(),
                    negociarItens.getItensMembroRebelde1(), negociarItens.getItensMembroRebelde2());
        } catch (Exception e) {
            Map<String, String> response = new HashMap<>();
            response.put(Mensagem.TEXT_MESSAGE, e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
        return ResponseEntity.ok().build();
    }
}
