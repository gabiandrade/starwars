package com.example.starwars.view.controller;


import com.example.starwars.domain.exception.BadRequestException;
import com.example.starwars.domain.exception.NotFoundException;
import com.example.starwars.domain.model.Localizacao;
import com.example.starwars.domain.model.Rebelde;
import com.example.starwars.data.service.impl.RebeldeServiceImpl;
import com.example.starwars.commons.utils.Mensagem;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/rebelde", produces = MediaType.APPLICATION_JSON_VALUE)
public class RebeldeController {

    private final RebeldeServiceImpl rebeldeServiceImpl;

    @PostMapping
    @ApiOperation(value = "Permite criar um rebelde")
    public ResponseEntity<Map<String, String>> criar(@RequestBody @Valid Rebelde rebelde) {
        try {
            rebeldeServiceImpl.criar(rebelde);
        } catch (BadRequestException e) {
            Map<String, String> response = new HashMap<>();
            response.put(Mensagem.TEXT_MESSAGE, e.getMessage());
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/{id}/localizacao")
    @ApiOperation(value= "Permite atualizar a localizacao de um rebelde com id especificado")
    public ResponseEntity<?> atualizarLocalizacao(@PathVariable Long id, @RequestBody Localizacao localizacao) {
        try {
            rebeldeServiceImpl.atualizarLocalizacao(id, localizacao);
        } catch (NotFoundException e) {
            Map<String, String> response = new HashMap<>();
            response.put(Mensagem.TEXT_MESSAGE, e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @GetMapping
    @ApiOperation(value = "Lista todos os rebeldes")
    public List<Rebelde> listar() {
        return rebeldeServiceImpl.listar();
    }

    @GetMapping("/{id}")
    @ApiOperation(value= "Permite recuperar um rebelde com id especificado")
    public ResponseEntity<?> recuperarPorId(@PathVariable Long id) {
        try {
            return ResponseEntity.ok().body(rebeldeServiceImpl.recuperarPorId(id));
        } catch (NotFoundException e) {
            Map<String, String> response = new HashMap<>();
            response.put(Mensagem.TEXT_MESSAGE, e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
    }

    @PostMapping("/{id}/reportar")
    @ResponseStatus(HttpStatus.OK)
    @ApiOperation(value = "reporta um rebelde como traidor")
    public void reportar(@PathVariable Long id) {
        rebeldeServiceImpl.reportar(id);
    }

}
