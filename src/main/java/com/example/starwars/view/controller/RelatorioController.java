package com.example.starwars.view.controller;

import com.example.starwars.domain.exception.NotFoundException;
import com.example.starwars.domain.dto.Relatorio;
import com.example.starwars.data.service.impl.RelatorioServiceImpl;
import com.example.starwars.commons.utils.Mensagem;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
@RestController
@RequestMapping(value = "/relatorio", produces = MediaType.APPLICATION_JSON_VALUE)
public class RelatorioController {

    private final RelatorioServiceImpl relatorioServiceImpl;

    @GetMapping("/porcentagem-traidores")
    @ApiOperation(value = "Gera um relatorio com a porcentagem de traidores")
    public Relatorio gerarRelatorioPorcentagemTraidores() {
        return relatorioServiceImpl.gerarRelatorioPorcentagemTraidores();
    }

    @GetMapping("/porcentagem-rebeldes")
    @ApiOperation(value = "Gera um relatorio com a porcentagem de rebeldes")
    public Relatorio gerarRelatorioPorcentagemRebeldes() {
        return relatorioServiceImpl.gerarRelatorioPorcentagemRebeldes();
    }

    @GetMapping("/recursos")
    @ApiOperation(value = "Gera um relatorio da quantidade de recursos utilizados por rebeldes ")
    public ResponseEntity<?> gerarRelatorioRecursos() {
        try {
            return ResponseEntity.ok().body(relatorioServiceImpl.gerarRelatorioRecursos());
        } catch (NotFoundException e) {
            Map<String, String> response = new HashMap<>();
            response.put(Mensagem.TEXT_MESSAGE, e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(response);
        }
    }

    @GetMapping("/pontos-perdidos")
    @ApiOperation(value = "Gera um relatorio da quantidade de pontos perdidos ")
    public Relatorio gerarRelatorioPontosPerdidos() {
        return relatorioServiceImpl.gerarRelatorioPontosPerdidos();
    }
}
