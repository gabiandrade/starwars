package com.example.starwars.commons.utils;

public class Mensagem {

    private Mensagem() {
    }

    public static final String TEXT_MESSAGE = "message";
    public static final String REBELDE_NAO_ENCONTRADO = "Rebelde não encontrado";
    public static final String RECURSO_INACESSIVEL="Recursos não encontrados ou inacessíveis";
    public static final String REBELDE_TRAIDOR = "O Rebelde '{rebelde}' é um traidor!";
    public static final String QTD_PONTOS_DIFERENTES = "Quantidade de pontos não podem ser diferentes!";
    public static final String ITENS_REBELDE_EXCEDE = "Itens para troca do rebelde '{rebelde}' excede"
            + " seu invetário!";
    public static final String QTD_ITEM_TROCA_MAIOR_QUE_INVETARIO = "A quantidade do item '{item}'"
            + " é maior do que a quantidade atual do inventário";
    public static final String TIPO_ITEM_INVALIDO = "Tipo de item inválido!";

    public static String getMessage(String key) {

        try {
            return Mensagem.class.getField(key).get(null).toString();
        } catch (Exception e) {
            return key;
        }
    }
}
